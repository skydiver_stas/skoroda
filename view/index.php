<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo $titlePage ?></title>
<meta name="description" content="Сайт Поетеси Скороди Юлії" />
<meta name="keywords" content="Скорода Юлія, Скорода Юлия, творчість, скорода" />
<link href="<?php echo LOCAL ;?>/template/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo LOCAL ;?>/template/javascript/jquery.js"></script>

<link rel="shortcut icon" href="<?php echo LOCAL ;?>/template/images/favicon.png" type="image/png">
</head>
<body>

<div id="wrapper">
  <div id="header">
    <div id="logo">
      <h1><a href="#">Юлія Скорода</a></h1>
      <h2><a href="http://www.freecsstemplates.org/">світ жіночої поезії</a></h2>
    </div>
    <!-- end div#logo -->
  </div>
  <!-- end div#header -->
  <div id="menu">
    <ul>
      <li><a href="<?php echo LOCAL ;?>/">Головна</a></li>
      <li><a href="<?php echo LOCAL ;?>/lyricsList/">Поезія</a></li>
      <li><a href="<?php echo LOCAL ;?>/stereotypes/">Стереотипи</a></li>
      <li><a href="<?php echo LOCAL ;?>/biography/">Біографія</a></li>
      <?php if(isset($_SESSION['admin'])) :?> <li><a href="<?php echo LOCAL ;?>/jadmin/">Адмін частина</a></li><?php endif; ?>
    </ul>
  </div>
  <!-- end div#menu -->
  <div id="page">
    <div id="page-bgtop">
      <div id="content">
     <?php if(isset($contentPath)) require_once($contentPath); ?>
     <?php if(isset($pagination)) echo $pagination->get(); ?>
     </div>
     </div>

 	  <!-- end div#content -->
      <div id="sidebar">
        <ul>
          <li id="search">
            <h2 class="search">Search</h2>
            <form method="get" action="">
              <fieldset>
              <input type="text" id="search-text" name="s" value="" />
              <input type="submit" id="search-submit" value="Search" />
              </fieldset>
            </form>
          </li>
          <li>
            <h2 class="categories">Поезія</h2>
            <ul>
            <?php foreach($categoryList as $key) : ?>
              <li><a href="<?php echo LOCAL ;?>/lyricsCat/<?php echo $key['sort_order']; ?>"><?php echo $key['name']; ?></a></li>
           <?php endforeach ; ?>
            </ul>
          </li>
          <li>
            <h2>Книжки</h2>
            <ul>
              <li><a href="<?php echo LOCAL ;?>/stereotypes/">Книга "Стереотипи"</a></li>

            </ul>
          </li>

        </ul>
      </div>
      <!-- end div#sidebar -->
      <div style="clear: both; height: 1px"></div>
    </div>
  </div>
  <!-- end div#page -->
  <div id="footer">
    <p>Copyright &copy; 2015 StasKO.&nbsp;&nbsp;<a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a></p>
  </div>
  <!-- end div#footer -->
</div>
<!-- end div#wrapper -->
</body>
</html>
