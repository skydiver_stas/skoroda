$(document).ready(function(){

	$("#add").click(function(){
		
		var title = $("#title").val ();
		var lyric = $("#lyric").val ();
		var category = $("#category").val ();
		var mes = [];
		var mess = '';
		var ver = true;

		if(title.length < 4 || title.length > 50)
		{
			mes[mes.length] = "Коротка або довга назва";
			ver = false;
		}

		if(lyric.length < 25)
		{
			mes[mes.length] = "Короткий текст";
			ver = false;
		}

		if(category == 0)
		{
			mes[mes.length] = "Не вибрано категорію";
			ver = false;
		}

		if(ver == false)
		{	
			$("#errors").empty();
			for (var i = 0; i < mes.length; i++) 
			{
				$("#errors").append("<li>"+mes[i]+"</li>");
			}
			return false;
		}

		$.ajax ({
			url:'/skorodaa/ajax/addLyric',
			type:"POST",
			data:{'title': title, 'lyric': lyric, 'category': category},
			dataType:'html',
			success: function(data)
			{
				if(data == "ok")
				{
					$("#errors").empty();
					mess = 'Додано';
					$("#errors").append("<li>"+mess+"</li>");
					setTimeout(function() {window.location.reload();}, 3000);
					return true;
				}

				else
				{
					$("#errors").empty();
					mess = 'Не додано';
					$("#errors").append("<li>"+mess+"</li>");
					return false;
				}	
				
			}
		});

	});
});