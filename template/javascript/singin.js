$(document).ready(function(){

	$("#auth").click(function(){
		var name = $("#name").val ();
		var password = $("#password").val ();
		var fail = [];
		var mes = '';
		var ver = true;

		console.log(name);

		var p=/^[a-z0-9_\.\-]{3,35}$/i;
		if(!p.test(name) || name.length < 2)
		{
			 fail[fail.length]="Incorrect Name";
			 ver = false;
		}	 
		
		if(!p.test(password) || password.length < 2)
		{
			 fail[fail.length]="Incorrect Password";
			 ver = false;
		}

		
		if(ver == false)
		{
			$("#errors").empty();
			for (var i = 0; i < fail.length; i++) 
			{
				$("#errors").append("<li>"+fail[i]+"</li>");
			}
			return false;
		
		}
			
			$.ajax ({
			url:'/skorodaa/ajax/singin',
			type:"POST",
			data:{'name': name, 'password': password},
			dataType: "html",
			success: function(data)
			{
				if(data == 'ok')
				{	
					$("#errors").empty();
					mes = 'Вітаємо вас ви успішно авторизувались!';
					$("#errors").append("<li>"+mes+"</li>");
					setTimeout(function() {window.location.reload();}, 3000);
					return true;
				}
				else 
				{	
					$("#errors").empty();
					mes = 'Такого користувача не існує';
					$("#errors").append("<li>"+mes+"</li>");
					return false;
				}

			}
		});
		
	});
});