<?php

	class LyricsController
	{

		public static function actionIndex($page = 1)
		{

			$categoryList = [];
			$categoryList = Category::getCategory();

			$lyricsList = [];
			$lyricsList = LyricsCategory::getLyricsList($page);

			$titlePage = 'Поезія';

			$total = LyricsCategory::getTotalLyrics();


			$pagination = new Pagination($total, $page, LyricsCategory::SHOW_BY_DEFAULT, 'page-');

			$contentPath = ROOT.'/view/listLyrics.php';
			require_once ROOT.'/view/index.php';
		}

			public static function actionlist()
		{

			$page = 1;

			$categoryList = [];
			$categoryList = Category::getCategory();

			$lyricsList = [];
			$lyricsList = LyricsCategory::getLyricsList($page);

			$titlePage = 'Поезія';

			$total = LyricsCategory::getTotalLyrics();


			$pagination = new Pagination($total, $page, LyricsCategory::SHOW_BY_DEFAULT, 'page-');

			$contentPath = ROOT.'/view/listLyrics.php';
			require_once ROOT.'/view/index.php';
		}

		public function actionCategory($id_category, $page = 1)
		{
			$categoryList = [];
			$categoryList = Category::getCategory();

			$lyricsList = [];
			$lyricsList = LyricsCategory::getLyricsCategoryList($id_category, $page);


			foreach ($categoryList as $key)
			{
				if($key['id'] == $id_category)
				{
					$titlePage = $key['name'];
				}
			}
			$total = LyricsCategory::getTotalCatalogCategory($id_category);


			$pagination = new Pagination($total, $page, LyricsCategory::SHOW_BY_DEFAULT, 'page-');

			$contentPath = ROOT.'/view/listLyrics.php';
			require_once ROOT.'/view/index.php';
		}

		public function actionCategoryList($id_category)
		{


			$page = 1;
			$categoryList = [];
			$categoryList = Category::getCategory();

			$lyricsList = [];
			$lyricsList = LyricsCategory::getLyricsCategoryList($id_category, $page);


			foreach ($categoryList as $key)
			{
				if($key['id'] == $id_category)
				{
					$titlePage = $key['name'];
				}
			}
			$total = LyricsCategory::getTotalCatalogCategory($id_category);


			$pagination = new Pagination($total, $page, LyricsCategory::SHOW_BY_DEFAULT, 'page-');

			$contentPath = ROOT.'/view/listLyrics.php';
			require_once ROOT.'/view/index.php';
		}

		public function actionLyricAlone($id)
		{

			$categoryList = [];
			$categoryList = Category::getCategory();

			$lyricsList = [];
			$lyricsList = LyricsCategory::actionLyricAlone($id);

			if($lyricsList['id'] == $id)
			{
				$titlePage = $lyricsList['title'];
			}



			$contentPath = ROOT.'/view/aloneLyric.php';
			require_once ROOT.'/view/index.php';
		}



	}
?>