<?php

    Class CabinetController extends AdminAccess
    {
        public static function actionCabinet()
        {   
            $categoryList = [];
			$categoryList = Category::getCategory();

            $titlePage = 'Кабінет';
            $contentPath = ROOT.'/view/cabinet.php';
            require_once ROOT.'/view/index.php';
        }

        public static function actionAddLyric()
        {
            $categoryList = [];
			$categoryList = Category::getCategory();

            $contentPath = ROOT.'/view/add.php';

            $titlePage = 'Додавання';
            require_once ROOT.'/view/index.php';
        }
    }

?>
 
