<?php
	class LyricsHomePage
	{	
		const SHOW_BY_DEFAULT = 4;
		public static function getHomePage()
		{

			$db = Db::getConnect();
			$limit = self::SHOW_BY_DEFAULT;
			$result = $db->query("SELECT id, title, text_lyric FROM db_lyrics ORDER BY RAND () LIMIT {$limit}");

			$lyricsList = $result->fetchAll();
			return $lyricsList;
		}
	}

?>