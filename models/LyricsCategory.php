<?php
	
	class LyricsCategory
	{
		const SHOW_BY_DEFAULT = 5;
		public static function getLyricsList($page = 1)
		{
			$page = intval($page);
			$limit = self::SHOW_BY_DEFAULT;
			$offset = ($page - 1) * $limit;

			$db = Db::getConnect();
			$limit = self::SHOW_BY_DEFAULT;
			$result = $db->query("SELECT id, title, text_lyric 
									FROM db_lyrics 
									ORDER BY id 
									DESC LIMIT {$limit}
									OFFSET {$offset} ");

			$lurycsList = $result->fetchAll();
			return $lurycsList;
		}

		public static function getLyricsCategoryList($id, $page = 1)
		{
			$page = intval($page);
			$limit = self::SHOW_BY_DEFAULT;
			$offset = ($page - 1) * $limit;
			$db = Db::getConnect();
			
			$result = $db->query("SELECT id, title, text_lyric 
									FROM db_lyrics 
									WHERE id_category = {$id} 
									ORDER BY id DESC 
									LIMIT {$limit} 
									OFFSET {$offset}");

			$lurycsList = $result->fetchAll();
			return $lurycsList;
		}

		public static function actionLyricAlone($id)
		{
			$db = Db::getConnect();

			$result = $db->query("SELECT id, title, text_lyric 
									FROM db_lyrics 
									WHERE id = {$id}");

			$lyric = $result->fetch();
			return $lyric;
		}

		public static function getTotalCatalogCategory($id_category)
		{
			$db = Db::getConnect();

			$result = $db->query("SELECT count(id) AS count
			FROM db_lyrics
			WHERE id_category = {$id_category}
			");

		$result->setFetchMode(PDO::FETCH_ASSOC);
		$row = $result->fetch();

		return $row['count'];
		}

			public static function getTotalLyrics()
		{
			$db = Db::getConnect();

			$result = $db->query("SELECT count(id) AS count
			FROM db_lyrics
			
			");

		$result->setFetchMode(PDO::FETCH_ASSOC);
		$row = $result->fetch();

		return $row['count'];
		}
	
		
	}

?>