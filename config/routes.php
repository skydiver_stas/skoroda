<?php
	return [
			
			ROUTE.'biography' => 'biography/index',
			ROUTE.'stereotypes' => 'stereotypes/index',

			// routes for ajax request
            ROUTE.'ajax/singin' => 'ajax/singIn',
            ROUTE.'ajax/addLyric' => 'ajax/addLyric',

            // end routes for ajax request

            // beginning routes for admin cabinet
            ROUTE.'jadmin/cabinet' => 'cabinet/cabinet',
            ROUTE.'jadmin/addLyric' => 'cabinet/addLyric',
            ROUTE.'jadmin' => 'enter/login',
			// end routes for admin cabinet

            ROUTE.'add' => 'add/index',

			ROUTE.'lyric/([0-9]+)' => 'lyrics/lyricAlone/$1',
			ROUTE.'lyricsCat/([0-9]+)/page-([0-9]+)' => 'lyrics/category/$1/$2',
			ROUTE.'lyricsCat/([0-9]+)' => 'lyrics/categoryList/$1',

			ROUTE.'lyricsList/page-([0-9]+)' => 'lyrics/index/$1',
			ROUTE.'lyricsList' => 'lyrics/list',
			ROUTE.'ytdu' => 'site/index'

			
			]
?>