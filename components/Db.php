<?php
	class Db
	{
		public static function getConnect()
		{
			$settingsPath = ROOT.'/config/settings_db.php';
			$settings_db = require $settingsPath;

			$dsn = "mysql:host={$settings_db['host']};dbname={$settings_db['dbname']}";
			$db = new PDO($dsn, $settings_db['user'], $settings_db['password']);
			$db->query("SET NAMES 'utf8'");
            
			return $db;
		}

	}

?>