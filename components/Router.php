<?php
	
	class Router
	{
		private $routes;
		public function __construct()
		{
			$routes = ROOT.'/config/routes.php';
			$this->routes = include$routes;
		} 

		private function getUri()
		{
			if(!empty($_SERVER['REQUEST_URI']))
			{
				return trim($_SERVER['REQUEST_URI'], '/');
			}

		}

		public function run()
		{
			$uri = $this->getUri();
			
			
			foreach($this->routes as $uriPattern => $path)
			{
				if(preg_match("~$uriPattern~", $uri))
				{	
					$string = preg_replace("~$uriPattern~", $path, $uri);
					$segment = explode('/', $string);
					break;
				}
			
			}	
				if(!isset($segment))
				{
					$segment = ['site', 'index'];
				}	

					$controllerName = ucfirst(array_shift($segment)).'Controller';
					$actionName = 'action'.ucfirst(array_shift($segment));
					$controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
	
					$parametr = $segment;
					
					if(file_exists($controllerFile))
					{
						include_once($controllerFile);
						$controllerObject = new $controllerName;
						$result = call_user_func_array(array($controllerObject, $actionName), $parametr);
						
						
			}
		}
	}
?>