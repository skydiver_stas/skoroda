<?php

	function __autoload($class_name)
	{
		$array_paths = [
			'/components/',
			'/controllers/',
			'/models/'
		];

		foreach ($array_paths as $path) {
			$path = ROOT.$path.$class_name.'.php';

			if(is_file($path))
			{
				include $path;
			}
		}
	}
?>